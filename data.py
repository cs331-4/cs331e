import requests, json

def load_leagues():
    url = "https://api-football-beta.p.rapidapi.com/leagues"
    headers = {
        'x-rapidapi-host': "api-football-beta.p.rapidapi.com",
        'x-rapidapi-key': "da94858498mshb5a91d3b63ec8d2p14e293jsn1f16aec6e292"
        }
    # getting German, British, French, Spanish, and Italian leagues
    leagues =  [{"id":"78","season":"2019"}, {"id":"39","season":"2019"}, {"id":"61","season":"2019"}, {"id":"141","season":"2019"}, {"id":"135","season":"2019"}]
    league_data = {}
    league_data['list'] = []

    for i in leagues:
        response = requests.request("GET", url, headers=headers, params=i)
        assert(response.status_code >= 200 and response.status_code < 300)
        league_data['list'].append(response.json())

    with open('league_data.json', 'w') as outfile:
        json.dump(league_data, outfile)

def load_teams():
    url = "https://api-football-beta.p.rapidapi.com/teams"
    headers = {
        'x-rapidapi-host': "api-football-beta.p.rapidapi.com",
        'x-rapidapi-key': "da94858498mshb5a91d3b63ec8d2p14e293jsn1f16aec6e292"
        }
    # getting German, British, French, Spanish, and Italian league teams
    leagues =  [{"league":"78","season":"2019"}, {"league":"39","season":"2019"}, {"league":"61","season":"2019"}, {"league":"141","season":"2019"}, {"league":"135","season":"2019"}]
    team_data = {}
    team_data['list'] = []

    for i in leagues:
        response = requests.request("GET", url, headers=headers, params=i)
        assert(response.status_code >= 200 and response.status_code < 300)
        team_data['list'].append(response.json())

    with open('team_data.json', 'w') as outfile:
        json.dump(team_data, outfile)

def load_json(filename):
    with open(filename) as file:
        jsn = json.load(file)
    return jsn

def list_teams(k):
    td = load_json('team_data.json')
    team_ids = []

    for i in td["list"][k]["response"]:
        team_ids.append(i['team']['id'])
    team_ids = [{"team":x,"season":"2019"} for x in team_ids]

    return team_ids

def load_players():
    url = "https://api-football-beta.p.rapidapi.com/players"
    headers = {
        'x-rapidapi-host': "api-football-beta.p.rapidapi.com",
        'x-rapidapi-key': "c041b48207msh92c6f9bc02d59dep11b47djsndae918e2ff41"
        }
    # getting German, British, French, Spanish, and Italian league teams
    leagues =  [{"league":"78","season":"2019"}, {"league":"39","season":"2019"}, {"league":"61","season":"2019"}, {"league":"141","season":"2019"}, {"league":"135","season":"2019"}]
    player_data = {}
    player_data['list'] = []

    for i in range(len(leagues)):
        team_list = list_teams(i)
        for j in team_list:
            response = requests.request("GET", url, headers=headers, params=j)
            assert(response.status_code >= 200 and response.status_code < 300)
            player_data['list'].append(response.json())
            
    with open('player_data.json', 'w') as outfile:
        json.dump(player_data, outfile)

if __name__ == "__main__":
    load_leagues()
    load_teams()
    load_players()
