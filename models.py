from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os

# in order to run, soccerdb database must be created in postgres
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING",'postgres://postgres:password@localhost:5432/soccerdb')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db = SQLAlchemy(app, session_options={'autocommit': False})

class League(db.Model):
    __tablename__ = 'League'

    league_id = db.Column(db.Integer, primary_key = True)
    league_name = db.Column(db.String(80), index = True)
    league_logo = db.Column(db.String(160))
    league_country = db.Column(db.String(20), index = True)
    league_country_flag = db.Column(db.String(160))
    league_season_start = db.Column(db.String(80), index = True)
    league_season_end = db.Column(db.String(80), index = True)

class Team(db.Model):
    __tablename__ = 'Team'

    team_id = db.Column(db.Integer, primary_key = True, index = True)
    team_name =  db.Column(db.String(80), index = True)
    team_logo = db.Column(db.String(160))
    team_country = db.Column(db.String(20), index = True)
    team_founding = db.Column(db.Integer)
    team_venue = db.Column(db.String(80))
    team_venue_capacity = db.Column(db.Integer)

    team_league_id = db.Column(db.Integer, db.ForeignKey('League.league_id'))

    coach_id= db.Column(db.Integer)
    coach_name = db.Column(db.String)

class Player(db.Model):
    __tablename__ = 'Player'

    # note that player_id and player_team_id form a composite PK, since load_players
    # appear on multiple teams
    player_id = db.Column(db.String(80), primary_key = True, index = True)
    player_name = db.Column(db.String(80), index = True)
    player_age = db.Column(db.Integer)
    player_nationality = db.Column(db.String(80), index = True)
    player_height = db.Column(db.String(40))
    player_weight = db.Column(db.String(40))
    player_injured = db.Column(db.Boolean, index = True)

    player_league_id = db.Column(db.Integer, db.ForeignKey('League.league_id'))
    player_team_id = db.Column(db.Integer, db.ForeignKey('Team.team_id'), primary_key = True)

    birth_date = db.Column(db.String(20))
    birth_place = db.Column(db.String(40), index = True)
    birth_country = db.Column(db.String(80), index = True)

db.drop_all()
db.create_all()
