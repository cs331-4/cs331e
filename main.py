from flask import Flask, render_template, request, redirect, url_for, jsonify, escape
from flask_bootstrap import Bootstrap
from create_db import app, db, League, Team, Player, create_leagues, create_teams, create_players
import random
import json
import os
import sys

bootstrap = Bootstrap(app)


@app.route('/', methods=['GET', 'POST'])
@app.route('/home/', methods=['GET', 'POST'])
def index():
    if request.method=='POST':
        search_value = request.form['search_value']
        league = db.session.query(League.league_id,League.league_name).filter(League.league_name == search_value).all()
        team = db.session.query(Team.team_id,Team.team_name).filter(Team.team_name == search_value).all()
        player = db.session.query(Player.player_id,Player.player_name).filter(Player.player_name == search_value).all()
        return render_template('index.html', league_data=league, team_data=team, player_data=player)
    else:
        return render_template('index.html', league_data=None, team_data=None, player_data=None)


@app.route('/league/', defaults={'league_id': None})
@app.route('/league/<league_id>', methods=['GET', 'POST'])
def league(league_id):
    if league_id is None:
        league = db.session.query(League.league_id,
                                  League.league_name,
                                  League.league_logo,
                                  League.league_country,
                                  League.league_country_flag,
                                  League.league_season_start,
                                  League.league_season_end).all()
        col_list = League.__table__.columns.keys()
        return render_template('league.html', league_data=league, column_list=col_list, team_list=None, player_list=None)
    else:
        league = db.session.query(League.league_id,
                                  League.league_name,
                                  League.league_logo,
                                  League.league_country,
                                  League.league_country_flag,
                                  League.league_season_start,
                                  League.league_season_end).filter(League.league_id == league_id).all()
        team = db.session.query(Team.team_id, Team.team_name, Team.team_logo).filter(
            Team.team_league_id == league_id).all()
        player = db.session.query(Player.player_id, Player.player_name).filter(
            Player.player_league_id == league_id).all()
        col_list = League.__table__.columns.keys()
        return render_template('league.html', league_data=league, column_list=col_list, team_list=team, player_list=player)


@app.route('/team/', defaults={'team_id': None})
@app.route('/team/<team_id>', methods=['GET', 'POST'])
def team(team_id):
	if team_id is None:
		team = db.session.query(Team.team_id,
								Team.team_name,
								Team.team_logo,
								Team.team_country,
								Team.team_founding,
								Team.team_venue,
								Team.team_venue_capacity,
								Team.team_league_id,
								Team.coach_id,
								Team.coach_name).all()
		col_list = Team.__table__.columns.keys()
		team_new = team.copy()
		for i in range(len(team)):
				league_name = db.session.query(League.league_name).filter(League.league_id == team_new[i][7]).first()
				temp = list(team_new[i])
				temp[7] = league_name[0]
				team_new[i] = tuple(temp)
		return render_template('team.html', column_list = col_list, team_data = team_new, player_list = None, league=league_name[0][0], team_data_raw = team)
	else:
		team = db.session.query(Team.team_id,
								Team.team_name,
								Team.team_logo,
								Team.team_country,
								Team.team_founding,
								Team.team_venue,
								Team.team_venue_capacity,
								Team.team_league_id,
								Team.coach_id,
								Team.coach_name).filter(Team.team_id == team_id).all()
		league_name = db.session.query(League.league_name).filter(League.league_id == team[0][7]).first()
		player = db.session.query(Player.player_id, Player.player_name).filter(Player.player_team_id == team_id).all()
		col_list = Team.__table__.columns.keys()
		return render_template('team.html', column_list = col_list, team_data = team, player_list = player, league = league_name, team_data_raw = team)


@app.route('/player/', defaults={'player_id': None})
@app.route('/player/<player_id>', methods=['GET', 'POST'])
def player(player_id):
	if player_id is None:
		player = db.session.query(Player.player_id,
								  Player.player_name,
								  Player.player_age,
								  Player.player_nationality,
								  Player.player_height,
								  Player.player_weight,
								  Player.player_injured,
								  Player.player_league_id,
								  Player.player_team_id,
								  Player.birth_date,
								  Player.birth_place,
								  Player.birth_country).all()
		col_list = Player.__table__.columns.keys()
		player_new = player.copy()
		for i in range(len(player)):
				league_name = db.session.query(League.league_name).filter(League.league_id == player_new[i][7]).first()
				team_name = db.session.query(Team.team_name).filter(Team.team_id == player_new[i][8]).first()
				temp = list(player_new[i])
				temp[7] = league_name[0]
				temp[8] = team_name[0]
				player_new[i] = tuple(temp)
		return render_template('player.html', column_list = col_list, player_data = player_new, league=None, team=None, player_data_raw = player)
	else:
		player = db.session.query(Player.player_id,
								  Player.player_name,
								  Player.player_age,
								  Player.player_nationality,
								  Player.player_height,
								  Player.player_weight,
								  Player.player_injured,
								  Player.player_league_id,
								  Player.player_team_id,
								  Player.birth_date,
								  Player.birth_place,
								  Player.birth_country).filter(Player.player_id == player_id).all()
		col_list = Player.__table__.columns.keys()
		league_name = db.session.query(League.league_name).filter(League.league_id == player[0][7]).first()
		team_name = db.session.query(Team.team_name).filter(Team.team_id == player[0][8]).first()
		return render_template('player.html', column_list = col_list, player_data = player, league=league_name[0], team=team_name[0], player_data_raw = player)

@app.route('/about/', methods=['GET', 'POST'])
def about():
    output = ''
    if request.method == 'POST':
        stream = os.popen('coverage run tests.py')
        stream2 = os.popen('coverage report -m')
        output = stream2.readlines()
    return render_template('about.html', output = output)

if __name__ == '__main__':
    #app.debug = True
    #app.run(host='0.0.0.0', port=5000)
    app.run()
