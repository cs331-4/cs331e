import json
from models import app, db, League, Team, Player

def load_json(filename):
    with open(filename) as file:
        jsn = json.load(file)
    return jsn

def create_leagues():
    league = load_json('league_data.json')

    for lg in league['leagues']:
        league_id = lg['response'][0]['league']['id']
        league_name = lg['response'][0]['league']['name']
        league_logo = lg['response'][0]['league']['logo']
        league_country = lg['response'][0]['country']['name']
        league_country_flag = lg['response'][0]['country']['flag']
        league_season_start = lg['response'][0]['seasons'][0]['start']
        league_season_end = lg['response'][0]['seasons'][0]['end']

        newLeague = League(league_id=league_id,
                           league_name=league_name,
                           league_logo=league_logo,
                           league_country=league_country,
                           league_country_flag=league_country_flag,
                           league_season_start=league_season_start,
                           league_season_end=league_season_end)

        db.session.add(newLeague)
        db.session.commit()

def create_teams():
    league = load_json('league_data.json')
    teams = load_json('team_data.json')

    # team_league_list relates teams and leagues so that they can be linked on
    # the player table easily
    team_league_list = {}
    k = 0
    for lg in teams['list']:
        for tm in lg['response']:
            team_id = tm['team']['id']
            team_name = tm['team']['name']
            team_logo = tm['team']['logo']
            team_country = tm['team']['country']
            team_founding = tm['team']['founded']
            team_venue = tm['venue']['name']
            team_venue_capacity = tm['venue']['capacity']

            team_league_id = league['leagues'][k]['response'][0]['league']['id']
            team_league_list[str(team_id)] = str(team_league_id)

            coach_id = tm['coach']['id']
            coach_name = str(tm['coach']['firstname']) + ' ' + str(tm['coach']['lastname'])

            newTeam = Team(team_id=team_id,
                           team_name=team_name,
                           team_logo=team_logo,
                           team_country=team_country,
                           team_founding=team_founding,
                           team_venue=team_venue,
                           team_venue_capacity=team_venue_capacity,
                           team_league_id=team_league_id,
                           coach_id=coach_id,
                           coach_name=coach_name)

            db.session.add(newTeam)
            db.session.commit()
        k += 1

    return team_league_list

def create_players(team_league_dict):
    players = load_json('player_data.json')

    for tm in players['list']:
        team_num = tm['parameters']['team']
        league_num = team_league_dict[str(team_num)]
        for pl in tm['response']:
            player_id = pl['player']['id']
            player_name = pl['player']['name']
            player_age = pl['player']['age']
            player_nationality = pl['player']['nationality']
            player_height = pl['player']['height']
            player_weight = pl['player']['weight']
            player_injured = pl['player']['injured']

            player_league_id = league_num
            player_team_id = team_num

            birth_date = pl['player']['birth']['date']
            birth_place = pl['player']['birth']['place']
            birth_country = pl['player']['birth']['country']

            newPlayer = Player(player_id=player_id,
                               player_name=player_name,
                               player_age=player_age,
                               player_nationality=player_nationality,
                               player_height=player_height,
                               player_weight=player_weight,
                               player_injured=player_injured,
                               player_league_id=player_league_id,
                               player_team_id=player_team_id,
                               birth_date=birth_date,
                               birth_place=birth_place,
                               birth_country=birth_country)

            db.session.add(newPlayer)
            db.session.commit()

create_leagues()
tl_list = create_teams()
create_players(tl_list)
