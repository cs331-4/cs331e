from unittest import main, TestCase
from create_db import app, db, League, Team, Player, create_leagues, create_teams, create_players
#from models import db, League, Team, Player

#need to create a delete data per test case
#because their are forign keys, all data entries need to be made for every single test
#this is fine because dependency tests can easily be run

def makeMockData():
    # league > team > player
    testLeague = League(
        league_id = 3, #db.Column(db.Integer, primary_key = True)
        league_name = "Iron IV", #db.Column(db.String(40), index = True)
        league_logo = "https://media.api-sports.io/leagues/8.png", #db.Column(db.String(40))
        league_country = "Land of bad", #db.Column(db.String(20), index = True)
        league_country_flag = "https://media.api-sports.io/flags/de.svg",#db.Column(db.String(40))
        league_season_start = "2019-08-16", #db.Column(db.String(40), index = True)
        league_season_end = "2020-05-16" #db.Column(db.String(40), index = True)
    )

    db.session.add(testLeague)
    db.session.commit()

    testTeam = Team(
        team_id = 1,
        team_name = "Toxic Bois", #db.Column(db.String(40), index = True)
        team_logo = "https://media.api-sports.io/teams/159.png", #db.Column(db.String(40))
        team_country = "Land of bad", #db.Column(db.String(20), index = True)
        team_founding = 2001, #db.Column(db.Integer)
        team_venue = "Stadium of bad", #db.Column(db.String(40))
        team_venue_capacity = 10, #db.Column(db.Integer)

        team_league_id = 3,

        coach_id = 2,
        coach_name = "Feeding Yasuo Jungle"
    )

    db.session.add(testTeam)
    db.session.commit()

    testPlayer = Player(
        player_id = "012",
        player_name = "Flaming leona adc",
        player_age = 15,
        player_nationality = "Bad",
        player_height = "5'9 .. and A HALF",
        player_weight = "Definitely over",
        player_injured = True,

        player_league_id = 3,
        player_team_id = 1,

        birth_date = "01/01/05",
        birth_place = "City of bad",
        birth_country = "The land of bad"
    )

    db.session.add(testPlayer)
    db.session.commit()

def scrubMockData():
    db.session.query(Player).filter_by(player_id = "012").delete()
    db.session.commit()
    db.session.query(Team).filter_by(team_id = 1).delete()
    db.session.commit()
    db.session.query(League).filter_by(league_id = 3).delete()
    db.session.commit()

class CustomUnittest(TestCase):

    #test player data base
    def test_player(self):
        makeMockData()
        r = db.session.query(Player).filter_by(player_id = "012").first()
        self.assertEqual(r.player_id, "012")
        scrubMockData()
        db.session.query(Player).filter_by(player_id = "012").delete()
        db.session.commit()

    #test team data base
    def test_team(self):
        makeMockData()
        r = db.session.query(Team).filter_by(team_id = 1).first()
        self.assertEqual(r.team_id, 1)
        scrubMockData()
        db.session.query(Team).filter_by(team_id = 1).delete()
        db.session.commit()

    #test league database
    def test_league(self):
        makeMockData()
        r = db.session.query(League).filter_by(league_id = 3).first()
        self.assertEqual(r.league_id, 3)
        scrubMockData()

        db.session.query(League).filter_by(league_id = 3).delete()
        db.session.commit()

if __name__ == '__main__':
    main()
